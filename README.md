## Usages 

The app requires python3.

```bash
python3 app.py

Enter organization name: <name>
Enter value of n: <value>
Enter value of m: <value>
```

### Sample Output
```bash
Enter organization name: google
Enter value of n: 3
Enter value of m: 2

1) Repository: google/it-cert-automation-practice

Top Committees:
1. marga-google, 3 commits
2. margamanterola, 1 commits


2) Repository: google/styleguide

Top Committees:
1. IsaacG, 30 commits
2. eglaysher, 25 commits


3) Repository: google/guava

Top Committees:
1. cpovirk, 1420 commits
2. kluever, 563 commits
```
