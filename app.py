import github as g

def get_input():
    print('Enter organization name:', end=' ')
    org = input()
    print('Enter value of n:', end=' ')
    n = int(input())
    print('Enter value of m:', end=' ')
    m = int(input())
    print()

    return org, n, m


def print_output(org: str, n: int, m: int) -> None:
    i = 1
    for repo in g.getRepoByFork(org, n):
        print(f'{i}) Repository: {repo["full_name"]}\n')
        i += 1

        committees = g.getCommittees(repo['full_name'], m)
        j = 1
        print('Top Committees:')
        for committee in committees:
            print(f'{j}. {committee["login"]}, {committee["contributions"]} commits')
            j += 1
        print('\n')

org, n, m = get_input()
print_output(org, n, m)