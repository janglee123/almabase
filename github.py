import requests

def getRepoByFork(org: str, n: int) -> list:
    url = f'https://api.github.com/search/repositories?q=user:{org}&sort=forks&order=desc&per_page={n}'
    
    result = []
    try:
        res = requests.get(url)
        
        if res.status_code == 200:
            data = res.json()
            result = list(data['items'])
        else:
            print(f'Something went wrong. Http error code {res.status_code}')

    except Exception as e:
        print(f'Something went wrong! {e}')
    
    return result


def getCommittees(repo_full_name: str, m: int) -> list:
    url = f'https://api.github.com/repos/{repo_full_name}/contributors?per_page={m}'
    
    result = []
    
    try:
        res = requests.get(url)
        if res.status_code == 200:
            data = res.json()
            result = data
        else:
            print(f'Something went wrong. Http error code {res.status_code}')

    except Exception as e:
        print(f'Something went wrong! {e}')
    
    return result


